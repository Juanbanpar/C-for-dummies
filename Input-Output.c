#include<stdio.h>   // Standard Input-Output library*/
#include<stdlib.h>  // Standard C library*/

int main(){
    printf("Hello world!\n"); 
    /*Printf prints in the same line, 
    * add "\n" to print on different lines
    */
    
    char str[100];
    int i;

    //The printf() and scanf()
    printf("Enter a a value: ");    
    scanf("%d",&i); 
    //&i represents the address of the whole array,
    //i represents the address of the first element of the array 
    
    printf("\nYou entered: %d", i);
    
    /* "%d" is a placeholder, it literraly 
     * "holds the place" for a number
     * format:
     * %d   - int
     * %ld  - long int
     * %f   - float
     * %lf  - double
     * %c   - char
     * %s   - string
     * %x   - hexadecimal
     */
    
    //The  getchar() and putchar()
    char c;
    printf("\nEnter a value: ");
    c = getchar();

    printf("\nYou entered: ");
    putchar(c);
    
    //The gets() and puts()
    printf("\nEnter a value: ");
    //gets(str); it's been deprecated
    fgets(str, 30, stdin);
    //str is a pointer to an array of chars
    //30 is the maximun number of chars readed
    //stdin is where characters are read from

    printf("\nYou entered: ");
    puts(str);
    return 0;
}

