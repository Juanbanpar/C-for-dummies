#include<stdio.h>                   //This is how you import packages

int main(){                         //main is a function needed in C, this is the code that will be run
  printf("Hello world!\n");         //printf is the function to print stuff
  return 0;                         //As main returns an int value just return 0 by default
}
