#include<time.h>
#include<stdio.h>
#include<stdlib.h>


int main(){

    int chances = 10;                                                                  //Total chances

    srand(time(NULL));                                                                 //Seed to create a diferent random each time
    int number = rand()%50;
    
    int guess;                                                                         //My answers
    char answer;

    do{
        printf("Do you want to guess mi secret number (between 0 and 50)? [y/n]: ");
        scanf("%c", &answer);                                                          //Scan the user's answer
        if(answer=='y'){                                                            
            printf("Awesome!!\n");
            printf("But here is a little twist...\n");
            printf("You only get 10 chances\n\n");
            do{
                printf("%i: Try to guess my number> ", chances);                        //Print the current chance
                scanf("%i", &guess);                                                    //To scan the guess you need the ampersand in order to tell C where you want to store the input
                if(guess>number){
                    printf("Your guess is higher than my number!\n\n");
                }else if(guess<number){
                    printf("Your guess is lower than my number...\n\n");
                }else{
                    printf("\nThat's it!!\n");
                    printf("You got it!!\n");
                    chances = 11-chances;
                    printf("You only needed %i chances\n", chances);                    //Print the total chances
                }
                chances--;
            }while(guess!=number && chances!=0);
            if(guess!=number && chances<=0){
                printf("Better luck next time...\n");
            }  
        }else if(answer=='n'){
            printf("What a pitty, maybe next time...\n");
        }else{
            printf("\nSorry, try again...\n");
        }
    }while(answer!='y' && answer!='n');


    return 0;

}
