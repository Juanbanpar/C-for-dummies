# C for dummies
Let's learn C collaboratively and save hours watching tutorials on YouTube

We are following these tutorials:<br />
https://www.tutorialspoint.com/cprogramming/c_program_structure.htm<br />
https://www.youtube.com/watch?v=-CpG3oATGIs

## Linux
Download the gcc package:
```
Ubuntu: sudo apt-get install gcc
Arch: sudo pacman -S gcc
```
To compile and create an executable:
```
gcc -o name_of_executable file.c
```
To run executable:
```
./name_of_executable
```

## Windows
Download the following program and add bin to PATH:<br />
https://sourceforge.net/projects/tdm-gcc/<br />
<br />
To compile:
```
gcc file.c
```
To run:
```
a
```


By [Fortesque73](https://github.com/Fortesque73), [Juan](https://github.com/hielo99) and [Alejandro de la Cruz](https://github.com/PaquitoElChocolatero)
